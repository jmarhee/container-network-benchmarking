Kubernetes Overlay Network Performance Benchmarking
===

Runs an `iperf` server in a Pod, and an `iperf` client as a `Job` resource to benchmark the container network on your cluster. Exposes the server as `iperf-overlay-service:5001`. 

Running
---

Apply the manifest:

```
kubectl create -f iperf-overlay.yaml
```

Check logs for server to verify the server is running:

```
kubectl logs -l app=iperf-overlay-server
```

Verify the client `Job` output:

```
kubectl logs -l app=iperf-overlay-client
```

These will contain the server and client statistics for the `iperf` run, respectively. 

Other Benchmarking Options
---

This is intended to benchmark the performance of the SDN Overlay Network, however, this test can be re-run for the `host` network by adding the `hostNetwork: true` option to the server `Pod` and client `Job` specs--this, however, requires [a permissive PodSecurityPolicy](https://kubernetes.io/docs/concepts/policy/pod-security-policy/#host-namespaces).

One can run different types of tests by overriding the default behavior of the `jmarhee/iperf` container (starting a server using `iperf -s`), using the `command` key in the Pod and Job specs and [modifying the `iperf` benchmarking to be done](https://openmaniak.com/iperf.php), respectively. 

The image is available on Docker Hub under `jmarhee/iperf`, but can be built and run locally using the included Dockerfile. 

Resources
---

More on `iperf` can be found here:

- [IPERF The Easy Tutorial](https://openmaniak.com/iperf.php)
- [Network Throughput Testing with iperf](https://www.linode.com/docs/networking/diagnostics/install-iperf-to-diagnose-network-speed-in-linux/)

More on Kubernetes network modes and security policies can be found here:

- [How Does The Kubernetes Networking Work?](https://medium.com/@tao_66792/how-does-the-kubernetes-networking-work-part-1-5e2da2696701)
- [Pod Security Policy (Host Namespaces)](https://kubernetes.io/docs/concepts/policy/pod-security-policy/#host-namespaces)
- [Kubernetes security context, security policy, and network policy](https://sysdig.com/blog/kubernetes-security-psp-network-policy/)
